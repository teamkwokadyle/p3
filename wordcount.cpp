/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: help from tyler, learning how to use functions
 *bit bucket tutorial,
 *various posts onlines
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 5hr  43mins currently.
 *Sarena Tran
 *Christopher Kwok
 *Lisbeth Lazala
 *Jeffrey Delfin
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

// write this function to help you out with the computation.

unsigned long countWords(const string& s, set<string>& wl){
  unsigned long index =0;// hold value of previous " "
  unsigned long wordCount = 0;
  for(unsigned long i = 1; i<s.length(); i++)
 {

  if((s[i] == ' ' || s[i] == '\t') && s[i-1] != ' ' && s[i-1] != '\t')
   {//checks if there is a space or a tab, and if there is no space before the word
     wordCount++;
     wl.insert(s.substr(index, i-index));
     index = i+1;
   }

  else if (i == s.length() - 1 && s[i-1] != ' ' && s[i-1] != '\t')
    {
      wordCount++;
      wl.insert(s.substr(index));
    }

  else if ((s[i] == ' ' || s[i] == '\t') && (s[i-1] != ' ' || s[i-1] != '\t'))
        {
          index = i+1; // iterates index to beginning of next word
        }


  }

   return wordCount;
  }


int main()
{

 set<string> uniqueWords; //string set for unique words
 set<string> uniqueLines;

 unsigned long wordCount = 0;
 unsigned long uWordCount = 0;
 unsigned long uLineCount = 0; //counter for unique line counts

  int numchars=0,numlines=0;
	string input;

	while(getline(cin,input)) //loops through input until done
   {

    if(input.length() != 0)

     {
       wordCount += countWords(input, uniqueWords); //iterates wordCount by reading both input and the string set uniqueWords

     }

    uWordCount = uniqueWords.size(); //sets unique word count to the size of the set unique words

		for (int i=0;i<input.size();i++)
    {
      numchars++; //character count
    }

    uniqueLines.insert(input); //only unique elements added to sets
    uLineCount = uniqueLines.size(); //size of set counts each unique line
    numchars++; //newlines count as a character
    numlines++; //countlines

   }

  cout<<numlines<<" "<<wordCount<<" "<<numchars<< " " <<uLineCount<< " "<< uWordCount << endl;

return 0;

}
